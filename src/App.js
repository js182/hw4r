import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import axios from 'axios';
import {setProducts} from './actions';
import './App.css';
import ProductList from './components/ProductList/ProductList';
import Header from './components/Header/Header';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import FavoritesList from './components/Favorites/FavoritesList';
import BasketList from './components/Cart/BasketList';

const App = () => {
  const dispatch = useDispatch();
  const [cartItems, setCartItems] = useState([]);
  const [cartCount, setCartCount] = useState(0);
  const [favoritesCount, setFavoritesCount] = useState(0);

  useEffect(() => {
    axios
      .get('products.json')
      .then((response) => {
        if (Array.isArray(response.data)) {
          dispatch(setProducts(response.data));
        } else {
          console.error('Data from products.json is not an array');
        }
      })
      .catch((error) => {
        console.error(`Error loading data from products.json: ${error}`);
      });
  }, [dispatch]);

  useEffect(() => {
    loadCartItems();
  }, []);

  const loadCartItems = () => {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    setCartItems(cart.length);
  };

  return (
    <Router>
      <div className="App">
        <Header cartCount={cartCount} favoritesCount={favoritesCount}/>
        <Routes>
          <Route
            path="/"
            element={
              <ProductList
                setFavoritesCount={setFavoritesCount}
                setCartCount={setCartCount}
              />
            }
          />
          <Route path="/cart" element={<BasketList setCartCount={setCartCount}/>}/>
          <Route
            path="/favorites"
            element={<FavoritesList setFavoritesCount={setFavoritesCount}/>}
          />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
