import React, {useEffect, useState} from 'react';
import ProductCard from '../ProductCard/ProductCard';
import s from './cart.module.scss';
import {openModal} from "../../modalActions";
import {useDispatch} from "react-redux";

const BasketList = ({setCartCount}) => {
  const [cartItems, setCartItems] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    setCartItems(cart);
  }, []);


  const handleRemoveFromCart = (skuToRemove) => {
    const newCart = cartItems.filter(product => product.sku !== skuToRemove);
    setCartItems(newCart);
    setCartCount(newCart.length)
    localStorage.setItem('cart', JSON.stringify(newCart));
  };
  const openModalHandler = () => {
    dispatch(openModal());
  }

  return (
    <div className={s.wrap}>
      {cartItems.map((product) => (
        <ProductCard
          notAddCard
          key={product.sku}
          product={product}
          openModalHandler={openModalHandler}
          removeFromCart={handleRemoveFromCart}/>
      ))}
    </div>
  );
};

export default BasketList;