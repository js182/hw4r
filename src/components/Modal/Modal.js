import React from "react";
import {connect} from "react-redux";
import {closeModal} from "../../modalActions";
import "./modal.scss";

class Modal extends React.Component {

  handleOutsideClick = (e) => {
    if (e.target === e.currentTarget) this.props.closeModal();
  };

  render() {
    const {header, closeButton, text, actions, isOpen} = this.props;
    return (
      <div
        className={`ModalOverlay ${isOpen ? "open" : ""}`}
        onClick={this.handleOutsideClick}
      >
        <div className="Modal">
          <div className="ModalHeader">
            {header}
            {closeButton && <button onClick={this.props.closeModal}>X</button>}
          </div>
          <div className="ModalBody">{text}</div>
          <div className="ModalFooter">{actions}</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  isOpen: state.modal.isOpen,
});

const mapDispatchToProps = (dispatch) => {
  return {
    closeModal: () => dispatch(closeModal()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
