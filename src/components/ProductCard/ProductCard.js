import React, {useEffect, useState} from 'react';
import s from "./styles.module.scss";
import Modal from "../Modal/Modal";
import {ReactComponent as StarIcon} from "../../svg/favorites.svg";
import PropTypes from "prop-types";
import {useDispatch, useSelector} from "react-redux";
import {closeModal} from "../../modalActions";

const ProductCard = (
  {
    product,
    removeFromFavorites,
    setFavoritesCount,
    setCartCount,
    removeFromCart,
    openModalHandler,
    notAddCard = false
  }
) => {
  const [selected, setSelected] = useState(false);
  const isModalOpen = useSelector((state) => state.modal.isOpen);
  const dispatch = useDispatch();

  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setSelected(favorites.some(favoriteProduct => favoriteProduct.sku === product.sku));
  }, [product]);

  const handleToggleFavorite = () => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    const productIndex = favorites.findIndex(item => item.sku === product.sku);

    if (productIndex !== -1) {
      removeFromFavorites(product.sku);
    } else {
      favorites.push(product);
      setFavoritesCount(favorites.length);
      localStorage.setItem('favorites', JSON.stringify(favorites));
    }
    setSelected(!selected);
  };

  const handleAddToCart = () => {
    openModalHandler();
  };

  const handleRemoveFromCart = () => {
    removeFromCart(product.sku);
  };

  const handleConfirm = () => {
    let cart = localStorage.getItem("cart");
    if (cart) {
      cart = JSON.parse(cart);
    } else {
      cart = [];
    }
    cart.push(product);
    setCartCount(cart.length);
    localStorage.setItem("cart", JSON.stringify(cart));
    dispatch(closeModal());
  };

  return (
    <div className={s.wrap}>
      <img src={product.image} alt={product.title}/>
      <h2 className={s.title}>{product.title}</h2>
      <p className={s.price}>Price: {product.price}</p>
      <p className={s.sku}> Art: {product.sku}</p>
      <p className={s.color}>{product.color}</p>
      <div className={s.footerCard}>
        {!notAddCard && <button className={s.cardbutton} onClick={handleAddToCart}>
          Add to cart
        </button>}
        {removeFromCart && (
          <button className={s.cardbutton} onClick={handleRemoveFromCart}>
            Remove from cart
          </button>
        )}
        {!removeFromCart && (
          <StarIcon
            className={`${s.favoriteStyle} ${selected ? s.selected : ''}`}
            onClick={handleToggleFavorite}
          />
        )}
      </div>
      {isModalOpen && (
        <Modal
          header="Confirmation"
          closeButton={true}
          text="Are you sure you want to add this product to your cart?"
          actions={<button onClick={handleConfirm}>Yes</button>}
          onClose={handleConfirm}
        />
      )}
    </div>
  );
};


ProductCard.propTypes = {
  product: PropTypes.shape({
    sku: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,
  removeFromFavorites: PropTypes.func,
  removeFromCart: PropTypes.func,
  openModalHandler: PropTypes.func,
};

export default ProductCard;
