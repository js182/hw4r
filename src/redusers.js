import {combineReducers} from 'redux';
import productsReducer from './ProductReducers';
import modalReducer from './modalReducer';

const rootReducer = combineReducers({
  products: productsReducer,
  modal: modalReducer,
});

export default rootReducer;